/*index.js
    JS part of Assignment 3 

    Revision History
        Pierre Loubateres, 2020.11.20: Final Version*/
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const {check, validationResult} = require('express-validator'); 

var myApp = express();
myApp.use(bodyParser.urlencoded({ extended:false }));
myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');

var phoneRegex = /^[0-9]{3}[\s\-]?[0-9]{3}[\s\-]?[0-9]{4}$/;
var creditCardRegex = /^[0-9]{4}[\s\-][0-9]{4}[\s\-][0-9]{4}[\s\-][0-9]{3,4}$/;
var emailRegex = /^[0-9A-Za-z.]{3,32}[\@][A-Za-z]{2,16}[\.][A-Za-z]{2,6}$/;
var passwordRegex = /^[0-9A-Za-z]{6,16}$/;

function checkRegex(userInput, regex){
    if(regex.test(userInput)){
        return true;
    }
    else{
        return false;
    }
}

function phoneValidation(value){
    if(!checkRegex(value, phoneRegex)){
        throw new Error('Your phone number should be in the following format XXX-XXX-XXXX')
    }
        return true;
}

function creditCardValidation(value){
    if(!checkRegex(value, creditCardRegex)){
        throw new Error('Your credit card number should be in the following format XXXX-XXXX-XXXX-XXX(X)')
    }
        return true;
}

function emailValidation(value){
    if(!checkRegex(value, emailRegex)){
        throw new Error('Your email should be in the following format test@test.com')
    }
        return true;
}

function passwordValidation(value){
    if(!checkRegex(value, passwordRegex)){
        throw new Error('Your password should be 6 to 16 alphanumerical characters')
    }
        return true;
}

function confPasswordValidation(value, {req}){
    var confPassword = req.body.confPassword;
    var password = req.body.password;
    if(confPassword == password){
        return true;
    }
    else{
        throw new Error('The passwords are not matching!');
    }
}

myApp.get('/', function(req, res){ 
    res.render('form');
});

myApp.post('/', [
    check('fullName', 'Please enter your full name').notEmpty(),
    check('phoneNumber', '').custom(phoneValidation),
    check('address', 'Please enter your address').notEmpty(),
    check('city', 'Please enter your city').notEmpty(),
    check('province', 'Please enter your province').notEmpty(),
    check('creditCard', '').custom(creditCardValidation),
    check('email', '').custom(emailValidation),
    check('password', '').custom(passwordValidation),
    check('confPassword','').custom(confPasswordValidation)
],function(req, res){

    var costIApple = 0;
    var costPersapple = 0;
    var costDicedApple = 0;
    var taxes = 0;
    var TAXEALB = 0.05;
    var TAXEBC = 0.12;
    var TAXEMAN = 0.12;
    var TAXENB = 0.15;
    var TAXENL = 0.15;
    var TAXENT = 0.05;
    var TAXENS = 0.15;
    var TAXENUN = 0.05;
    var TAXEONT = 0.13;
    var TAXEPEI = 0.15;
    var TAXEQC = 0.14975;
    var TAXESASK = 0.11;
    var TAXEYUK = 0.05;

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        res.render('form', {
            errors:errors.array()
        });
    }
    else{
        var fullName = req.body.fullName;
        var phoneNumber = req.body.phoneNumber;
        var address = req.body.address;
        var city = req.body.city;
        var province = req.body.province;
        var creditCard = req.body.creditCard;
        var iApple = req.body.iApple;
        var persapple = req.body.persapple;
        var dicedApple = req.body.dicedApple;

        if(iApple == 1){
            costIApple = 999.99;
        }
        else if(iApple == 2){
            costIApple = 2499.99;
        }
        else if(iApple == 3){
            costIApple = 3499.99;
        }
    
        if(persapple == 1){
            costPersapple = 24.99;
        }
        else if(persapple == 2){
            costPersapple = 44.99;
        }
        else if(persapple == 3){
            costPersapple = 64.99;
        }
    
        if(dicedApple == 1){
            costDicedApple = 4.99;
        }
        else if(dicedApple == 2){
            costDicedApple = 8.99;
        }
        else if(dicedApple == 3){
            costDicedApple = 12.99;
        }

        if(province == 'Alberta'){
            taxes = TAXEALB;
        }
        else if(province == 'British Columbia'){
            taxes = TAXEBC;
        }
        else if(province == 'Manitoba'){
            taxes = TAXEMAN;
        }
        else if(province == 'New-Brunswick'){
            taxes = TAXENB;
        }
        else if(province == 'Newfoundland and Labrador'){
            taxes = TAXENL;
        }
        else if(province == 'Northwest Territories'){
            taxes = TAXENT;
        }
        else if(province == 'Nova Scotia'){
            taxes = TAXENS;
        }
        else if(province == 'Nunavut'){
            taxes = TAXENUN;
        }
        else if(province == 'Ontario'){
            taxes = TAXEONT;
        }
        else if(province == 'Prince Edward Island'){
            taxes = TAXEPEI;
        }
        else if(province == 'Quebec'){
            taxes = TAXEQC;
        }
        else if(province == 'Saskatchewan'){
            taxes = TAXESASK;
        }
        else if(province == 'Yukon'){
            taxes = TAXEYUK;
        }

        subTotal = costIApple + costPersapple + costDicedApple;
        total = subTotal + (subTotal*taxes);

        var pageData = {
            fullName : fullName,
            phoneNumber : phoneNumber, 
            address : address,
            city : city,
            province : province,
            creditCard : creditCard,
            iApple : iApple,
            persapple : persapple,
            dicedApple : dicedApple,
            subTotal : subTotal,
            taxes : taxes,
            total : total
        }

        res.render('form', pageData);
    }
});

myApp.listen(8888);
console.log('Wow look at that amazing website: http://localhost:8888');