How to install the website:

1.Download Node.js on https://nodejs.org/en/download/
2.In your terminal in VSCode type 'npm install'
3.Once it is done, type 'node index.js'
4.Go to http://localhost:8888
5.Enjoy the website

I chose the APACHE License because each term is defined very clearly from the start to allow a better understanding. The text is clear and understandable.